library(readr)
library(ggplot2)
my.df <- read_csv("~/school/cse6230/proj1/all_cum.csv", col_names = FALSE)
colnames(my.df) <- c("Interval", "Time","Threads")
ggplot(my.df,
       aes(x=Interval, y=Time, ymin=0,
           group=Threads, label=Threads, color=Threads)
       ) + geom_line() + geom_point() + scale_y_sqrt()
ggsave("proj1-cumulative.png", width=14, height=7)
