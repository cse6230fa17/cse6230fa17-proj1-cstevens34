#include "box.h"
#include <cmath>
#include <assert.h>
#include <iostream>
#include <omp.h>

int debug = 0;

__thread Random *Box::_rng;

Box::Box(Point dimensions, float particle_radius) : _dimensions(dimensions), _celldim(0,0,0)
{
    const float R = sqrt(4 * particle_radius * particle_radius);

    _celldim.x = _celldim.y = _celldim.z = R;
    _cols = ceil(dimensions.x / R);
    _rows = ceil(dimensions.y / R);
    _levels = ceil(dimensions.z / R);

    if ( _cols * R > dimensions.x)
    {
        _cols--;
        _celldim.x = dimensions.x/(_cols-1);
    }
    if ( _rows * R > dimensions.y)
    {
        _rows--;
        _celldim.y = dimensions.y/(_rows-1);
    }
    if ( _levels * R > dimensions.z)
    {
        _levels--;
        _celldim.z = dimensions.z/(_levels-1);
    }

    // printf("box dimensions %d, %d, %d\n", _cols, _rows, _levels);
    // exit(0);

    _cells.reserve(_cols * _rows * _levels);
    for (int z = 0; z < _levels; z++)
        for (int y = 0; y < _rows; y++)
            for (int x = 0; x < _cols; x++)
                _cells.push_back(Cell(Point(x * _celldim.x, y * _celldim.y, z * _celldim.z), _celldim));
}

void Box::init(int size, int seed)
{
    _particles.reserve(size + 1);

    // insert bad particle at position 0
    _particles.push_back(Particle());

    // initialize a random number generator for each thread
    // each generator is seeded with a number from the "main" RNG
    // this avoids the bias that each generator is generating the same
    // sequence of numbers.  It also should help reproducability,
    // although there is no guarantee that the same threads will be seeded
    // with the same number since thread order cannot be guaranteed between
    // runs
    Random seeder(seed);
#pragma omp parallel
    {
#pragma omp critical
        {
            int s = seeder();
            _rng = new Random(s);
            _vectors[omp_get_thread_num()].clear();
        }
    }
    // this is a maximum number of pairs
    // this will allocate ~200 meg for 10,000 particles
    // resize this memory here to avoid locking contention
    // use the resize operation here instead of reserve since we
    // want to be able to use the index ( [] ) operation, but
    // that means we need to track the size of the vector ourself.
    // pairs.resize(size, ParticlePair(0,0));
    //

    const double factor = 50;
    // estimate particle pairing based on a uniform density, then multiple by some factor for worst-case
    // actual number for maximally dense would be size * (size - 1) /2 but this can be grossly over
    // the actual memory required for computation
    int n = size * (size - 1) * (27.0 / _cells.size()) * factor;
    std::cout << "allocating " << n << " space for computations" << std::endl;
    pairs.resize(n); // size * (size - 1) / 2);
    std::vector< bool > visited( _cells.size(), false);

    // precalculate the list of adjacent cells
    // this step can be precalculated since the list of adjacent cell is static
    // gather work step
    // this step is single threaded since there is contention amongst resources
    // the main contention is the calculation of which particles have already
    // been visited i.e. which particles have had (or will have) a complete force calculation 
    // performed.  We want to ensure that we do not duplicate the calculation on these particles.
    // during this step, each cell is visited in order, and then the cell (and the particles it contains)
    // is marked as "visited" which means that subsequent iterations will not pull those cells and hence 
    // the particles within that cell.
    worklist.resize(_cells.size());
    for (int i = 0; i < _cells.size(); i++)
    {
        std::vector<int>& cells = worklist[i]; 
        int x = this->x(i);
        int y = this->y(i);
        int z = this->z(i);
        for (int zp = -1; zp < 2; zp ++)
            for (int yp = -1; yp < 2; yp ++)
                for (int xp = -1; xp < 2; xp ++)
                {
                    int z_ = z + zp;
                    int y_ = y + yp;
                    int x_ = x + xp;

                    /* I think your method doesn't take into account cells
                     * that are adjacent accross periodic boundaries */
                    if (z_ >= 0 && z_ < _levels && 
                        y_ >= 0 && y_ < _rows && 
                        x_ >= 0 && x_ < _cols)
                    {
                        int cell = get_index(x_,y_,z_);
                        if (!visited[cell])
                        {
                            cells.push_back(cell);
                        }
                    }
                }
        visited[i] = true;
    }
}

void Box::simulate(int time)
{
    // cannot parallelize this loop since each time step is dependant on the next
    for (int t = 0; t < time; t++)
        step();
}

void Box::step()
{
    int size = 0;

    // the gather_pairs function will calculate the size (#of pairs) for the next loop
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < worklist.size(); i++)
    {
        gather_pairs(pairs, size, worklist[i]);
    }

    // if (debug > 0) std::cout << "compute force" << std::endl;
    #pragma omp parallel for simd schedule(static)
    for (int i = 0; i < size; i++)
    {
        Particle* p1 = pairs[i].first;
        Particle* p2 = pairs[i].second;

	// calculate the distance between particles
        double dx = p1->position.x - p2->position.x;
        double dy = p1->position.y - p2->position.y;
        double dz = p1->position.z - p2->position.z;

	// determine force to be applied based on distance
        double dt = (dx * dx) + (dy * dy) + (dz * dz);
        double distance = sqrt(dt);
        if (distance < 2 )
        {
            double f = 100*(2-distance)/distance;
            p1->apply_force_branched(dx, dy, dz, f);
            p2->apply_force_branched(dx, dy, dz, -f);
        }
    }

    // if (debug > 0) std::cout << "clear cells" << std::endl;
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < _cells.size(); i++)
        _cells[i].clear();

    // if (debug > 0) std::cout << "move particles" << std::endl;
    #pragma omp parallel for simd schedule(static)
    for (int i = 1; i < _particles.size(); i++)
    {
        // add the guassian noise to particle movement.  Not sure what this should
        // be so a mean of 0 and deviation of 1 seems reasonable.
        NormalDistribution distribution(0, 1);
        Particle& p = _particles[i];
        const double DT = sqrt(2.0 * 1E-4);
        const Point vDT(DT,DT,DT);
        p.apply_force(vDT.x, vDT.y, vDT.z, distribution(*_rng));
        p.displace();

        if (!this->contains(p))
        {
            /* I emphasized in the very first version of the project
             * description, "periodicity is not enforced on the position
             * itself."  I even responded to you on Piazza: "*But*, we also
             * want to track the non-periodic distance that each particle
             * travels from its original position.  So we use non-periodic
             * coordinates for position and periodic coordinates for
             * interaction." */
            p.position.x = remainder(p.position.x - _dimensions.x/2, _dimensions.x) + _dimensions.x/2;
            p.position.y = remainder(p.position.y - _dimensions.y/2, _dimensions.y) + _dimensions.y/2;
            p.position.z = remainder(p.position.z - _dimensions.z/2, _dimensions.z) + _dimensions.z/2;
        }
        int x= p.position.x / _celldim.x;
        int y= p.position.y / _celldim.y;
        int z= p.position.z / _celldim.z;
        (*this)(x,y,z).add_particle(&p);
    }
}

void Box::gather_pairs(PairArray& pairs, int &size, std::vector<int>& cells)
{
    // gather particle pairing step.
    // this step collects all the particles from the current cell and all adjacent cells
    // then it computes the pairings of those particles for later force calculation
    //
    // fetch a pre-allocated vector of particles.  This helps optimize memory allocations over
    // repeated steps.
    std::vector<Particle*>& particles = _vectors[omp_get_thread_num()];
    particles.clear();

    for (int j = 0; j < cells.size(); j++)
    // for (std::vector<int>::iterator i = cells.begin(); i != cells.end(); i++)
    {
        int x = this->x(cells[j]);
        int y = this->y(cells[j]);
        int z = this->z(cells[j]);
        assert(cells[j] < _cells.size());
        // if (debug > 2) std::cout << "X: get cell at " << cells[j] << " = " << x << ", " << y << ", " << z << std::endl;
        Cell &cell = (*this)(x,y,z);
        for (std::vector<Particle*>::iterator a = cell.begin(); a != cell.end(); a++)
            particles.push_back(*a);
    }

    int n = particles.size() * (particles.size()-1) / 2;
    int offset;
// #pragma omp atomic capture (g++ 4.4 doesn't support this directive)
#pragma omp atomic capture
    { 
        offset = size; size += n; 
        // if (size > pairs.capacity()) pairs.resize(size);
    }
    // if (debug > 1) std::cout << "gathered: " << size << " -> " << n << std::endl;
    n = 0;
    for (int i = 0; i < particles.size(); i++)
        for (int j = i + 1; j < particles.size(); j++)
        {
            assert(particles[i]);
            assert(particles[j]);
            pairs[offset + n] = ParticlePair(particles[i], particles[j]);
            assert(pairs[offset + n].first);
            assert(pairs[offset + n].second);
            n++;
        }
}
