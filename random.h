#if !defined(RANDOM_H)
#define RANDOM_H

#include <cmath>
// source taken from Intel with permission
// https://software.intel.com/en-us/articles/fast-random-number-generator-on-the-intel-pentiumr-4-processor/

class UniformDistribution
{
private:
    double _min, _max;

public:
    inline UniformDistribution(double min = 0, double max = 1) : _min(min), _max(max) {}

    template <typename T> double operator()(T& rng)
    {
		return static_cast<double>(rng()) / rng.max * (_max - _min) + _min;
    }
};

class NormalDistribution
{
private:
    double _mean;
    double _deviation;
    double _x, _y;
    double _state;

public:
    inline NormalDistribution(double mean, double deviation) : _mean(mean), _deviation(deviation) {}

    template <typename T> double operator()(T& rng)
    {
        double U1, U2, W, mult;
        if (_state == 1)
        {
            _state = !_state;
            return (_mean + _deviation * (double) _y);
        }
        else
        {
            /* This is the Marsaglia polar form of normal random number
             * generation.  That while loop, which is predicated on a random
             * number, will be impossible for branch prediction to get right
             * more than 50% of the time, leading to a lot of stalls.  Is it
             * faster than the Box-Muller transform I implemented for you?
             * Did you test this to find out?
             */
            do
            {
                U1 = -1 + ((double) rng() / rng.max) * 2;
                U2 = -1 + ((double) rng() / rng.max) * 2;
                W = pow(U1, 2) + pow(U2, 2);
            } while (W >= 1 || W == 0);
            mult = sqrt ((-2 * log (W)) / W);
            _x = U1 * mult;
            _y = U2 * mult;
 
            _state = !_state;
        }
        return (_mean + _deviation * (double) _x);
    }
};

class Random
{
private:
    int _bits;

public:
    static const int max = 0x7FFF;

    Random(double seed = 0) : _bits(seed) {}

    inline double operator()()
    { 
          /* This is cheap linear congruential generator that should never be
           * used for a scientific application where we need to trust the
           * validity of the statistics that we are generating.  I feel like I
           * devoted plenty of time in class to this, *and* you were using the
           * much better Mersenne Twister PRNG in an earlier version of the
           * code. */
          _bits = (214013*_bits+2531011); 
          return (_bits>>16)&0x7FFF; 
    } 
};

#endif
